const log4js = require('log4js');

let logger = log4js.getLogger();

logger.level = "debug";

logger.info("Info");
logger.warn("Warning");
logger.error("Error");
logger.fatal("Fatal");

const holaMundo = "Hola mundo!";
console.log(holaMundo);

function sumar(x, y){
    return x + y;
}

module.exports = sumar;