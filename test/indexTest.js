const sumar = require('../index');
const assert = require('assert');

// Assert = Afirmación
// Test 50%

describe("Probar la suma de dos números", ()=>{
    // Afirmamos que 5 + 5 = 10
    it("5 + 5 = 10", ()=>{
        assert.equal(10, sumar(5,5));
    });
    // Afirmamos que 5 + 7 != 10
    it("5 + 5 != 10", ()=>{
        assert.notEqual(10, sumar(5,7));
    });
});